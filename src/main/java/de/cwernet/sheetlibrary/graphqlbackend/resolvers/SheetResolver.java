package de.cwernet.sheetlibrary.graphqlbackend.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import de.cwernet.sheetlibrary.graphqlbackend.model.composer.Composer;
import de.cwernet.sheetlibrary.graphqlbackend.model.sheet.Sheet;
import de.cwernet.sheetlibrary.graphqlbackend.repositories.ComposerRepository;
import org.springframework.stereotype.Component;

@Component
public class SheetResolver implements GraphQLResolver<Sheet> {
    private ComposerRepository composerRepository;

    public SheetResolver(ComposerRepository composerRepository) {
        this.composerRepository = composerRepository;
    }

    public Composer getComposer(Sheet sheet){
        return composerRepository.findById(sheet.getComposerId()).orElse(new Composer("Default Composer"));
    }
}
