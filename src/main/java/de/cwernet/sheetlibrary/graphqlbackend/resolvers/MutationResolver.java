package de.cwernet.sheetlibrary.graphqlbackend.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import de.cwernet.sheetlibrary.graphqlbackend.model.composer.Composer;
import de.cwernet.sheetlibrary.graphqlbackend.model.composer.ComposerNotFoundException;
import de.cwernet.sheetlibrary.graphqlbackend.model.composer.CreateComposerInput;
import de.cwernet.sheetlibrary.graphqlbackend.model.composer.UpdateComposerInput;
import de.cwernet.sheetlibrary.graphqlbackend.model.sheet.CreateSheetInput;
import de.cwernet.sheetlibrary.graphqlbackend.model.sheet.Sheet;
import de.cwernet.sheetlibrary.graphqlbackend.model.sheet.SheetNotFoundException;
import de.cwernet.sheetlibrary.graphqlbackend.model.sheet.UpdateSheetInput;
import de.cwernet.sheetlibrary.graphqlbackend.model.user.CreateUserInput;
import de.cwernet.sheetlibrary.graphqlbackend.model.user.UpdateUserInput;
import de.cwernet.sheetlibrary.graphqlbackend.model.user.User;
import de.cwernet.sheetlibrary.graphqlbackend.model.user.UserNotFoundException;
import de.cwernet.sheetlibrary.graphqlbackend.repositories.ComposerRepository;
import de.cwernet.sheetlibrary.graphqlbackend.repositories.SheetRepository;
import de.cwernet.sheetlibrary.graphqlbackend.repositories.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class MutationResolver implements GraphQLMutationResolver {
    private SheetRepository sheetRepository;
    private ComposerRepository composerRepository;
    private UserRepository userRepository;

    public MutationResolver(SheetRepository sheetRepository, ComposerRepository composerRepository, UserRepository userRepository) {
        this.sheetRepository = sheetRepository;
        this.composerRepository = composerRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public Sheet createSheet(CreateSheetInput input) {
        return sheetRepository.saveAndFlush(new Sheet(input.getTitle(), input.getComposerId(), input.getLocationId()));
    }

    @Transactional
    public Sheet updateSheet(UpdateSheetInput input) {
        Sheet sheet = sheetRepository
                .findById(input.getId())
                .orElseThrow(() -> new SheetNotFoundException(input.getId()));
        sheet.setTitle(input.getTitle());
        sheet.setComposerId(input.getComposerId());
        sheet.setLocationId(input.getLocationId());
        return sheetRepository.saveAndFlush(sheet);
    }

    @Transactional
    public int deleteSheet(Long id) {
        if (sheetRepository.existsById(id)) {
            sheetRepository.deleteById(id);
            return id.intValue();
        } else {
            return -1;
        }
    }

    @Transactional
    public Composer createComposer(CreateComposerInput input) {
        return composerRepository.saveAndFlush(new Composer(input.getName()));
    }

    @Transactional
    public Composer updateComposer(UpdateComposerInput input) {
        Composer composer = composerRepository
                .findById(input.getId())
                .orElseThrow(() -> new ComposerNotFoundException(input.getId()));
        composer.setName(input.getName());
        return this.composerRepository.saveAndFlush(composer);
    }

    @Transactional
    public int deleteComposer(Long id) {
        if (composerRepository.existsById(id)) {
            composerRepository.deleteById(id);
            return id.intValue();
        } else {
            return -1;
        }
    }

    @Transactional
    public User createUser(CreateUserInput input) {
        return this.userRepository.saveAndFlush(new User(input.getName(), input.getUsername(), input.getEmail(), input.getRole(), input.getPassword()));
    }

    @Transactional
    public User updateUser(UpdateUserInput input) {
        User user = this.userRepository.findById(input.getId()).orElseThrow(() -> new UserNotFoundException(input.getId()));
        user.setName(input.getName());
        user.setUsername(input.getUsername());
        user.setEmail(input.getEmail());
        user.setPassword(input.getPassword());
        user.setRole(input.getRole());
        return this.userRepository.saveAndFlush(user);
    }

}
