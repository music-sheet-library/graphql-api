package de.cwernet.sheetlibrary.graphqlbackend.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import de.cwernet.sheetlibrary.graphqlbackend.model.composer.Composer;
import de.cwernet.sheetlibrary.graphqlbackend.model.sheet.Sheet;
import de.cwernet.sheetlibrary.graphqlbackend.model.user.User;
import de.cwernet.sheetlibrary.graphqlbackend.repositories.ComposerRepository;
import de.cwernet.sheetlibrary.graphqlbackend.repositories.SheetRepository;
import de.cwernet.sheetlibrary.graphqlbackend.repositories.UserRepository;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Component
public class QueryResolver implements GraphQLQueryResolver {

    private SheetRepository sheetRepository;
    private ComposerRepository composerRepository;
    private UserRepository userRepository;

    public QueryResolver(SheetRepository sheetRepository, ComposerRepository composerRepository, UserRepository userRepository) {
        this.sheetRepository = sheetRepository;
        this.composerRepository = composerRepository;
        this.userRepository = userRepository;
    }

    public List<Sheet> getSheets() {
        System.out.println("Trying to find all Sheets!");
        System.out.println(this.sheetRepository);
        System.out.println(this.sheetRepository.findAll());
        return sheetRepository.findAll();
    }

    public List<Composer> getComposers(){
        return composerRepository.findAll();
    }

    public Sheet getSheet(Long id) {
        return sheetRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Could not find sheet with id " + id.toString()));
    }

    public List<User> users() {
        return this.userRepository.findAll();
    }
}
