package de.cwernet.sheetlibrary.graphqlbackend.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import de.cwernet.sheetlibrary.graphqlbackend.model.composer.Composer;
import de.cwernet.sheetlibrary.graphqlbackend.model.sheet.Sheet;
import de.cwernet.sheetlibrary.graphqlbackend.repositories.SheetRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ComposerResolver implements GraphQLResolver<Composer> {
    private SheetRepository sheetRepository;

    public ComposerResolver(SheetRepository sheetRepository) {
        this.sheetRepository = sheetRepository;
    }

    public List<Sheet> getWorks(Composer composer){
        System.out.println("Find works of composer " + composer.getName());
        System.out.println(this.sheetRepository);
        System.out.println(this.sheetRepository.findAll());
        //System.out.println(this.sheetRepository.findAll().stream().filter(sheet -> sheet.getComposerId() == composer.getId()).collect(Collectors.toList()));
        return this.sheetRepository.findByComposerId(composer.getId());
    }

}
