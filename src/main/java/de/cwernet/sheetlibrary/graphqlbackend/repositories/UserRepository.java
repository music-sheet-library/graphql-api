package de.cwernet.sheetlibrary.graphqlbackend.repositories;

import de.cwernet.sheetlibrary.graphqlbackend.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
