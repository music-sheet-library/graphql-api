package de.cwernet.sheetlibrary.graphqlbackend.repositories;

import de.cwernet.sheetlibrary.graphqlbackend.model.composer.Composer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComposerRepository extends JpaRepository<Composer, Long> {
}
