package de.cwernet.sheetlibrary.graphqlbackend.repositories;

import de.cwernet.sheetlibrary.graphqlbackend.model.sheet.Sheet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SheetRepository extends JpaRepository<Sheet, Long> {
    List<Sheet> findByComposerId(Long composerId);

    List<Sheet> findByLocationId(Long locationId);
}
