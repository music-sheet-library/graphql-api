package de.cwernet.sheetlibrary.graphqlbackend.model.sheet;

import java.util.List;

public class CreateSheetInput {
    private String title;
    private Long composerId;
    private Long locationId;

    public CreateSheetInput() {
    }

    public CreateSheetInput(String title, Long composerId, Long locationId) {
        this.title = title;
        this.composerId = composerId;
        this.locationId = locationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getComposerId() {
        return composerId;
    }

    public void setComposerId(Long composerId) {
        this.composerId = composerId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }
}
