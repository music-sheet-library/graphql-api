package de.cwernet.sheetlibrary.graphqlbackend.model.sheet;

import java.util.List;

public class UpdateSheetInput {
    private Long id;
    private String title;
    private Long composerId;
    private Long locationId;

    public UpdateSheetInput() {
    }

    public UpdateSheetInput(Long id, String title, Long composerId, Long locationId) {
        this.id = id;
        this.title = title;
        this.composerId = composerId;
        this.locationId = locationId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getComposerId() {
        return composerId;
    }

    public void setComposerId(Long composerId) {
        this.composerId = composerId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationIds(Long locationId) {
        this.locationId = locationId;
    }
}
