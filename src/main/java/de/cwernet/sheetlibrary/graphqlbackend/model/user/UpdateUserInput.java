package de.cwernet.sheetlibrary.graphqlbackend.model.user;

public class UpdateUserInput {
    private Long id;
    private String name;
    private String username;
    private String email;
    private UserRole role;
    private String password;

    public UpdateUserInput() {
    }

    public UpdateUserInput(Long id, String name, String username, String email, UserRole userRole, UserRole role, String password) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.role = role;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole userRole) {
        this.role = userRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
