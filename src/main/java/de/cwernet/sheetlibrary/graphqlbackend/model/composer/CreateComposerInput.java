package de.cwernet.sheetlibrary.graphqlbackend.model.composer;

public class CreateComposerInput {
    private String name;

    public CreateComposerInput() {
    }

    public CreateComposerInput(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
