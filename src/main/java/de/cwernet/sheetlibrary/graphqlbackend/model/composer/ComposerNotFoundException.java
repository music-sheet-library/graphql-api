package de.cwernet.sheetlibrary.graphqlbackend.model.composer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ComposerNotFoundException extends RuntimeException implements GraphQLError {
    private Long composerId;

    public ComposerNotFoundException(Long composerId) {
        this.composerId = composerId;
    }

    @Override
    public String getMessage(){
        return "Composer with ID " + composerId + " could not be found.";
    }

    @Override
    @JsonIgnore
    public StackTraceElement[] getStackTrace(){
        return super.getStackTrace();
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.ValidationError;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return Collections.singletonMap("composerId", composerId);
    }
}
