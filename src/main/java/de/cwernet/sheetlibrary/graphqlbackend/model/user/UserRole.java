package de.cwernet.sheetlibrary.graphqlbackend.model.user;

public enum UserRole {
    CONDUCTOR,
    SHEETORGANISER,
    ADMIN,
    GUEST
}
