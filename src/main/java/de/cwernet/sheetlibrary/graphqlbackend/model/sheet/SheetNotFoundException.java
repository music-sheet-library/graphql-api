package de.cwernet.sheetlibrary.graphqlbackend.model.sheet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class SheetNotFoundException extends RuntimeException implements GraphQLError {
    private Long sheetId;

    public SheetNotFoundException(Long sheetId) {
        this.sheetId = sheetId;
    }

    @Override
    public String getMessage(){
        return "Sheet with ID " + sheetId + " could not be found.";
    }

    @Override
    @JsonIgnore
    public StackTraceElement[] getStackTrace(){
        return super.getStackTrace();
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.ValidationError;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return Collections.singletonMap("sheetId", sheetId);
    }
}
