package de.cwernet.sheetlibrary.graphqlbackend.model.sheet;

import javax.persistence.*;
import java.util.List;

@Table(name = "music_sheet")
@Entity
public class Sheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "composer_id", nullable = false)
    private Long composerId;
    @Column(name = "location_id", nullable = false)
    private Long locationId;

    public Sheet() {

    }

    public Sheet(String title, Long composerId, Long locationId) {
        this.title = title;
        this.composerId = composerId;
        this.locationId = locationId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getComposerId() {
        return composerId;
    }

    public void setComposerId(Long composerId) {
        this.composerId = composerId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }
}
