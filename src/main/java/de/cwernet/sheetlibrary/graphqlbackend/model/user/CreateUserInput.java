package de.cwernet.sheetlibrary.graphqlbackend.model.user;

public class CreateUserInput {
    private String name;
    private String username;
    private String email;
    private UserRole role;
    private String password;

    public CreateUserInput() {
    }

    public CreateUserInput(String name, String username, String email, UserRole role, String password) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.role = role;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
